cmake_minimum_required (VERSION 2.8.4)
project (Contingency)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++0x -O3")
add_subdirectory(src)
add_subdirectory(test)
enable_testing ()
