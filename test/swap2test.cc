#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <map>
#include <cassert>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include "shuffle2.h"

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::copy;
using std::vector;
using std::map;
using boost::random::mt19937;
using boost::random::uniform_int_distribution;

int main(int argc, char *argv[]) {
  vector< vector< int > > a = {{0, 0, 0, 0, 1, 1, 1, 1},
			       {0, 0, 1, 1, 0, 0, 0, 1}};
  vector< int > c(10);
  mt19937 gen;
  map< vector< int >, int > hits;

  for (int i = 0; i < 1000000; ++i) {
    shuffle2(&a[0][0], &a[1][0], 8, &c[0], gen);
    hits[a[0]]++;
  }
  assert(hits.size() == 10);
  for (auto pair = hits.begin(); pair != hits.end(); ++pair) {
    assert(abs(100000 - pair->second) < 10000);
  }
  vector< vector< int > > mat(10, vector< int >(10, 0));
  uniform_int_distribution<> coin(0, 1);
  for (int run = 0; run < 100; ++ run) {
    vector< int > rowsums(10, 0);
    vector< int > colsums(10, 0);
    for (int i = 0; i < 10; ++i) {
      for (int j = 0; j < 10; ++j) {
	int a = coin(gen);
	mat[i][j] = a;
	rowsums[i] += a;
	colsums[j] += a;
      }
    }
    for (int i = 0; i < 1000; ++i) {
      vector< int > currrow(10, 0);
      vector< int > currcol(10, 0);
      shufflemat2(&mat, 10, &c[0], gen);
      for (int row = 0; row < 10; ++row) {
	for (int col = 0; col < 10; ++col) {
	  currrow[row] += mat[row][col];
	  currcol[col] += mat[row][col];
	}
      }
      assert(currrow == rowsums);
      assert(currcol == colsums);
    }
  }
}
