#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <map>
#include <cassert>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <gmpxx.h>
#include "combos.h"
#include "shuffle3.h"

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::copy;
using std::vector;
using std::map;
using boost::random::mt19937;
using boost::random::uniform_int_distribution;

ostream_iterator< int > out_it(cout, " ");

int main(int argc, char *argv[]) {
  vector< vector< int > > a = {{0, 0, 1, 0, 1, 0, 1, 1},
			       {0, 0, 0, 1, 0, 1, 1, 1},
			       {0, 0, 0, 0, 1, 1, 1, 1}};
  vector< int > s1(12);
  vector< int > s2(12);
  vector< int > s3(12);
  mt19937 gen;
  map< vector< vector< int > >, int > hits;
  vector< vector< int > > combos = gen_combos< int >(10);

  for (int i = 0; i < 1500000; ++i) {
    shuffle3t< int >(&(a[0][0]), &(a[1][0]), &(a[2][0]), 8, &s1[0], &s2[0], 
		     &s3[0], combos, gen);
    hits[a]++;
  }
  assert(hits.size() == 15);
  // for (auto p : hits) {
  //   assert(abs(100000 - p.second) < 10000);
  // }

  vector< vector< int > > mat(10, vector< int >(10, 0));
  uniform_int_distribution<> coin(0, 1);
  for (int run = 0; run < 100; ++ run) {
    vector< int > rowsums(10, 0);
    vector< int > colsums(10, 0);
    for (int i = 0; i < 10; ++i) {
      for (int j = 0; j < 10; ++j) {
	int a = coin(gen);
	mat[i][j] = a;
	rowsums[i] += a;
	colsums[j] += a;
      }
    }
    for (int i = 0; i < 1000; ++i) {
      vector< int > currrow(10, 0);
      vector< int > currcol(10, 0);
      shufflemat3t< int >(&mat, 10, &s1[0], &s2[0], &s3[0], combos, gen);
      for (int row = 0; row < 10; ++row) {
	for (int col = 0; col < 10; ++col) {
	  currrow[row] += mat[row][col];
	  currcol[col] += mat[row][col];
	}
      }
      assert(currrow == rowsums);
      assert(currcol == colsums);
    }
  }
}
