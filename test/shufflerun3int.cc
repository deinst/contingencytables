#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <map>
#include <cassert>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <gmpxx.h>
#include "combos.h"
#include "shuffle3.h"

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::copy;
using std::vector;
using std::map;
using boost::random::mt19937;
using boost::random::uniform_int_distribution;

ostream_iterator< int > out_it(cout, " ");

int main(int argc, char *argv[]) {
  if (argc < 2) {
    cout << "Usage: " << argv[0] << " n" << endl;
    return 1;
  }

  int n = atoi(argv[1]);

  vector< int > s1(n + 2);
  vector< int > s2(n + 2);
  vector< int > s3(n + 2);
  mt19937 gen;

  vector< vector< int > > combos = gen_combos< int >(n);

  vector< vector< int > > mat(n, vector< int >(n, 0));
  uniform_int_distribution<> coin(0, 1);
  for (int run = 0; run < 100; ++ run) {
    vector< int > row(n, 0);
    vector< int > col(n, 0);
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
	int a = coin(gen);
	mat[i][j] = a;
	row[i] += a;
	col[j] += a;
      }
    }
    for (int i = 0; i < 1000; ++i) {
      shufflemat3t< int >(&mat, 10, &s1[0], &s2[0], &s3[0], combos, gen);
    }
    vector< int > rowc(n, 0);
    vector< int > colc(n, 0);
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
	rowc[i] += mat[i][j];
	colc[j] += mat[i][j];
      }
    }
    assert(row == rowc);
    assert(col == colc);
  }
}
