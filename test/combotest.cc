#include <vector>
#include <gmpxx.h>
#include <iostream>
#include <stdlib.h>
#include "combos.h"

using std::vector;
using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
  if (argc < 3) {
    cout << "Usage: " << argv[0] << " n k" << endl;
    return 1;
  }
  int n = atoi(argv[1]);
  int k = atoi(argv[2]);
  {
    vector< vector< mpz_class > > combos = gen_combos< mpz_class >(n);
    cout << get_combos< mpz_class >(combos, n, k) << endl;
  }
  return 0;
}
