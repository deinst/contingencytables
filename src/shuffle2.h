#ifndef _SHUFFLE2_H_
#define _SHUFFLE2_H_
#include <vector>
#include <boost/random/mersenne_twister.hpp>
using std::vector;
using boost::random::mt19937;
void shuffle2(int *a, int *b, int n, int *scratch, mt19937 &gen);

void shufflemat2(vector< vector< int > > *mat, int reps,
		 int *scratch, mt19937 &gen);


#endif /* _SHUFFLE2_H_ */
