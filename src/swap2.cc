#include <vector>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>

using std::vector;
using boost::random::mt19937;
using boost::random::uniform_int_distribution;

/* shuffle the two 0-1 sequences starting at a and b
   of length n.  Keeping the row and columns sums equal. 
   at least n units of scratch space
*/
void shuffle2(int *a, int *b, int n, int *scratch, mt19937 &gen) {
  int asum = 0;
  int bsum = 0;
  int onecount = 0;
  int twocount = 0;
  for (int i = 0; i < n; ++i) {
    switch (2*a[i] + b[i]) {
    case 1:
      scratch[onecount] = i;
      onecount++;
      bsum++;
      break;
    case 2:
      scratch[onecount] = i;
      onecount++;
      asum++;
      break;
    case 3:
      twocount++;
      asum++;
      bsum++;
      break;
    default:
      break;
    }   
  }
  int oneswap = asum - twocount;

  bool useb = false;
  if (2 * oneswap > onecount) {
    oneswap = onecount - oneswap;
    useb = true;
  }
  if (oneswap == 0) return;
  for (int i = onecount; i > onecount - oneswap; --i) {
    uniform_int_distribution<> dist(0, i - 1);
    int pos = dist(gen);
    int temp = scratch[pos];
    scratch[pos] = scratch[i - 1];
    if (useb) {
      b[temp] = 1;
      a[temp] = 0;
    } else {
      a[temp] = 1;
      b[temp] = 0;
    }
  }
  for (int i = 0; i < onecount - oneswap; ++i) {
    int temp = scratch[i]; 
    if (useb) {
      a[temp] = 1;
      b[temp] = 0;
    } else {
      b[temp] = 1;
      a[temp] = 0;
    }
  } 
}

void shufflemat2(vector< vector< int > > *mat,
		int reps, int *scratch, mt19937 &gen) {
  int rows = mat->size();
  int cols = (*mat)[0].size();
  uniform_int_distribution<> dist1(0, rows - 1);
  uniform_int_distribution<> dist2(0, rows - 2);
  for (int i = 0; i < reps; ++i) {
    int row1 = dist1(gen);
    int row2 = dist2(gen);
    if (row2 >= row1) row2++;
    shuffle2(&(*mat)[row1][0], &(*mat)[row2][0], cols, scratch, gen);
  }
}
