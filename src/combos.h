#ifndef _COMBOS_H_
#define _COMBOS_H_

#include <vector>
#include <gmpxx.h>

using std::vector;
template< typename T >
vector< vector< T > > gen_combos(unsigned int nmax) {
  vector< vector< T > > ret;
  vector< T > r0(2, 1);
  r0[1] = 0;
  ret.push_back(r0);
  vector< T > r1(2, 1);
  ret.push_back(r1);
  for (int i = 2; i <= nmax; ++i) {
    vector< T > row(i + 1, 1);
    for (int j = 1; j < i; ++j) {
      row[j] = ret[i - 1][j - 1] + ret[i - 1][j]; 
    }
    ret.push_back(row);
  }
  return ret;
}

template< typename T >
static inline const T &get_combos(const vector< vector< T > > &tab, 
				 unsigned int n, unsigned int k) {
  if (n < 0 || k < 0 || n < k) {
    return tab[0][1];
  } else {
    return tab[n][k];
  }
}

#endif /* _COMBOS_H_ */
