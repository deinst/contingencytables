#ifndef _SHUFFLE3_H_
#define _SHUFFLE3_H_

#include <vector>
#include <gmpxx.h>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>

using std::vector;
using boost::random::mt19937;
using boost::random::uniform_int_distribution;

/* shuffle the three 0-1 sequences starting at a, b, and c
   of length n.  Keeping the row and columns sums equal. 
   2 fragments of at least n units of scratch space
*/
void shuffle3(int *a, int *b, int *c, int n, int *s1, int *s2, mpz_class *s3,
	      const vector< vector< mpz_class> > &combos, gmp_randclass &rand, 
	      mt19937 &gen);

void shufflemat3(vector< vector< int > > *mat, int reps, 
		 int *s1, int *s2, mpz_class *s3, 
		 const vector< vector< mpz_class > > &combos,
		 gmp_randclass &rand, mt19937 &gen);

template< typename T >
void shuffle3t(int *a, int *b, int *c, int n, int *s1, int *s2, T *s3,
	      const vector< vector< T > > &combos, 
	      mt19937 &gen) {
  //first collect the row and column sums.
  int asum = 0;
  int bsum = 0;
  int csum = 0;
  int onecount = 0;
  int twocount = 0;
  int threecount = 0;
  for (int i = 0; i < n; ++i) {
    switch (4 * a[i] + 2 * b[i] + c[i]) {
    case 1:
      s1[onecount] = i;
      onecount++;
      csum++;
      break;
    case 2:
      s1[onecount] = i;
      onecount++;
      bsum++;
      break;
    case 3:
      s2[twocount] = i;
      twocount++;
      bsum++;
      csum++;
      break;
    case 4:
      s1[onecount] = i;
      onecount++;
      asum++;
      break;
    case 5:
      s2[twocount] = i;
      twocount++;
      asum++;
      csum++;
      break;
    case 6:
      s2[twocount] = i;
      twocount++;
      asum++;
      bsum++;
      break;
    case 7:
      threecount++;
      asum++;
      bsum++;
      csum++;
      break;
    default:
      break;
    }
  }
  // now count the number of or tables for each choce of first_avail
  static T total_so_far;
  static T curr;
  total_so_far = 0;
  int first_avail = asum - threecount;
  int second_avail = bsum - threecount;
  int range_max = std::min(first_avail, onecount);
  range_max = std::min(range_max, second_avail + first_avail - twocount);
  range_max = std::min(range_max, onecount + twocount - second_avail);
  // second_avail + first_avail - twocount >= first_ones
  // onecount + twocount - second_avail >= first_ones
  // onecount + twocount - second_avail >= first_ones
  int range_min = std::max(0, first_avail - twocount);
  for (int first_ones = range_min; first_ones <= range_max; ++first_ones) {
    int first_twos = first_avail - first_ones;
    int remain_ones = (onecount - first_ones) + first_twos;
    int remain_needed = second_avail - (twocount - first_twos);
    curr = get_combos< T >(combos, onecount, first_ones) * 
      get_combos< T >(combos, twocount, first_twos); 
    curr = curr * get_combos< T >(combos, remain_ones, remain_needed);
    total_so_far += curr;
    s3[first_ones] = total_so_far;
  } 
  // now choose one uniformly
  uniform_int_distribution< T > distc(0, total_so_far - 1);
  T choice = distc(gen);
  int first_ones = range_min;
  while (choice >= s3[first_ones]) first_ones++;
  // uniform_int_distribution<> dist(range_min, range_max);
  // int first_ones = dist(gen);
  int first_twos = first_avail - first_ones;
  // assign the first ones
  if (2 * first_ones <= onecount) {
    for (int i = 0; i < first_ones; ++i) {
      uniform_int_distribution<> dist(0, onecount - 1);
      int pos = dist(gen);
      int temp = s1[pos];
      s1[pos] = s1[onecount - 1];
      a[temp] = 1;
      b[temp] = 0;
      c[temp] = 0;
      onecount--;
    }
    for (int i = 0; i < onecount; ++i) {
      a[s1[i]] = 0;
    }
  } else {
    for (int i = 0; i < onecount - first_ones; ++i) {
      uniform_int_distribution<> dist(i, onecount - 1);
      int pos = dist(gen);
      int temp = s1[pos];
      s1[pos] = s1[i];
      s1[i] = temp;
      a[temp] = 0;
    }
    for (int i = onecount - first_ones; i < onecount; ++i) {
      a[s1[i]] = 1;
      b[s1[i]] = 0;
      c[s1[i]] = 0;
    }
    onecount -= first_ones;
  }
  if (2 * first_twos <= twocount) {
    for (int i = 0; i < first_twos; ++i) {
      uniform_int_distribution<> dist(0, twocount - 1);
      int pos = dist(gen);
      int temp = s2[pos];
      s2[pos] = s2[twocount - 1];
      a[temp] = 1;
      s1[onecount] = temp;
      onecount++;
      twocount--;
    }
    for (int i = 0; i < twocount;++i) {
      int temp = s2[i];
      a[temp] = 0;
      b[temp] = 1;
      c[temp] = 1;
    }
  } else {
    for (int i = 0; i < twocount - first_twos; ++i) {
      uniform_int_distribution<> dist(i, twocount - 1);
      int pos = dist(gen);
      int temp = s2[pos];
      s2[pos] = s2[i];
      s2[i] = temp;
      a[temp] = 0;
      b[temp] = 1;
      c[temp] = 1;
    }
    for (int i = twocount - first_twos; i < twocount; ++i) {
      a[s2[i]] = 1;
      s1[onecount] = s2[i];
      onecount++;
    }
    twocount -= first_twos;
  }
  int oneswap = bsum - threecount - twocount;
  if (2 * oneswap <= onecount) {
    for (int i = onecount; i > onecount - oneswap; --i) {
      uniform_int_distribution<> dist(0, i - 1);
      int pos = dist(gen);
      int temp = s1[pos];
      s1[pos] = s1[i - 1];
      b[temp] = 1;
      c[temp] = 0;
    }
    for (int i = 0; i < onecount - oneswap; ++i) {
      int temp = s1[i]; 
      b[temp] = 0;
      c[temp] = 1;
    } 
  } else {
    for (int i = 0; i < onecount - oneswap; ++i) {
      uniform_int_distribution<> dist(i, onecount - 1);
      int pos = dist(gen);
      int temp = s1[pos];
      s1[pos] = s1[i];
      b[temp] = 0;
      c[temp] = 1;
    }
    for (int i = onecount; i > onecount - oneswap; --i) {
      int temp = s1[i - 1]; 
      b[temp] = 1;
      c[temp] = 0;
    } 
  }
}


template< typename T > 
void shufflemat3t(vector< vector< int > > *mat, int reps, 
		 int *s1, int *s2, T *s3, 
		 const vector< vector< T > > &combos,
		  mt19937 &gen) {
  int rows = mat->size();
  int cols = (*mat)[0].size();
  uniform_int_distribution<> dist1(0, rows - 1);
  uniform_int_distribution<> dist2(0, rows - 2);
  uniform_int_distribution<> dist3(0, rows - 3);
  for (int i = 0; i < reps; ++i) {
    int row1 = dist1(gen);
    int row2 = dist2(gen);
    int row3 = dist3(gen);
    if (row2 >= row1) {
      row2++;
      if (row3 >= row1) {
	row3++;
	if (row3 >= row2) row3++;
      }
    } else {
      if (row3 >= row2) {
	row3++;
	if (row3 >= row1) row3++;
      }
    }
    shuffle3t< T >(&(*mat)[row1][0], &(*mat)[row2][0], &(*mat)[row3][0],
	     cols, s1, s2, s3, combos, gen);
  }
}

#endif /* _SHUFFLE3_H_ */
